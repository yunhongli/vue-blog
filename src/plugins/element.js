import ElementPlus from 'element-plus'
import '../element-variables.scss'
import locale from 'element-plus/lib/locale/lang/zh-cn'
import router from '../router/index'

export default (app) => {
  app.use(ElementPlus, { locale }).use(router)
}
