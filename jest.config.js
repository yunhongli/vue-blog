module.exports = {
  compilerOptions: {
    basUrl: './',
    paths: {
      '@/*': ['src/*'],
    },
  },
  preset: '@vue/cli-plugin-unit-jest',
}
