const path = require('path')
function resolve(dir) {
  return path.join(__dirname, dir)
}
const { VueLoaderPlugin } = require('vue-loader')
const HtmlPlugin = require('html-webpack-plugin')
const htmlPlugin = new HtmlPlugin({
  templateParameters: {
    BASE_URL: `/`,
  },
  template: './public/index.html', // 这是html模板存放的地址
  filename: './index.html',
})
module.exports = {
  mode: 'development',
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      { test: /\.vue$/, use: ['vue-loader'] },
      {
        test: /\.s[ca]ss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      { test: /\.(jpg|png|gif|bmp|jpeg)$/, loader: 'url-loader' },
      { test: /\.(ttf|eot|svg|woff|woff2)$/, use: 'url-loader' },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: path.resolve(__dirname, './webpackPlgin/delconsole.js'),
      },
    ],
  },
  resolve: {
    alias: {
      '@': resolve('src'),
    },
    extensions: ['.vue', '.js'],
  },
  // 插件就让多一个功能
  plugins: [new VueLoaderPlugin(), htmlPlugin],
  devServer: {
    open: true, //打包完自动打开文件
    host: '127.0.0.1',
    port: 8080, //实时打包所使用的端口号
    client: {
      logging: 'none',
    }, // 关闭客户端的log
  },
}
